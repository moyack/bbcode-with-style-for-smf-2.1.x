<?php

/**
 * BBCode with style
 *
 * @package BBCodeWithStyle21
 * @version 1.0
 * @author moyack, davidhs
 * @copyright 2010-2020 by davidhs, 2022 by moyack
 * @license Creative Commons Attribution 3.0 Unported License https://creativecommons.org/licenses/by/3.0/
 */

if (!defined('SMF'))
	die('Hacking attempt...');

// Adds bbc codes.
function bbcws_bbc_codes(&$codes)
{
	$codes[] = array(
		'tag' => 'img',
		'type' => 'unparsed_content',
		'parameters' => array(
			'alt' => array('optional' => true),
			'title' => array('optional' => true),
			'width' => array('optional' => true, 'value' => ' width="$1"', 'match' => '(\d+)'),
			'height' => array('optional' => true, 'value' => ' height="$1"', 'match' => '(\d+)'),
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' $1'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'content' => '$1',
		'validate' => function(&$tag, &$data, $disabled, $params)
				{
					$url = iri_to_url(strtr($data, array('<br>' => '')));

					if (parse_iri($url, PHP_URL_SCHEME) === null)
						$url = '//' . ltrim($url, ':/');
					else
						$url = get_proxied_url($url);

					$alt = !empty($params['{alt}']) ? ' alt="' . $params['{alt}']. '"' : ' alt=""';
					$title = !empty($params['{title}']) ? ' title="' . $params['{title}']. '"' : '';
					$class = !empty($params['{class}']) ? $params['{class}'] : '';
					$style = !empty($params['{style}']) ? $params['{style}'] : '';

					$data = isset($disabled[$tag['tag']]) ? $url : '<img src="' . $url . '"' . $alt . $title . $params['{width}'] . $params['{height}'] . ' class="bbc_img' . (!empty($params['{width}']) || !empty($params['{height}']) ? ' resized' : '') . $class . '"' . $style . ' loading="lazy">';
				},
		'disabled_content' => '($1)',
	);

	$codes[] = array(
		'tag' => 'div',
		'parameters' => array(
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' class="$1"'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'before' => '<div{class}{style}>',
		'after' => '</div>',
		'block_level' => true,
	);
	$codes[] = array(
		'tag' => 'hr',
		'type' => 'closed',
		'parameters' => array(
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' class="$1"'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'content' => '<hr{class}{style} />',
		'block_level' => true,
	);
	$codes[] = array(
		'tag' => 'li',
		'parameters' => array(
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' class="$1"'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'before' => '<li{class}{style}>',
		'after' => '</li>',
		'trim' => 'outside',
		'require_parents' => array('list'),
		'block_level' => true,
		'disabled_before' => '',
		'disabled_after' => '<br />',
	);
	$codes[] = array(
		'tag' => 'list',
		'parameters' => array(
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' $1'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'before' => '<ul class="bbc_list{class}"{style}>',
		'after' => '</ul>',
		'trim' => 'inside',
		'require_children' => array('li', 'list'),
		'block_level' => true,
	);
	$codes[] = array(
		'tag' => 'list',
		'parameters' => array(
			'type' => array('match' => '(none|disc|circle|square|decimal|decimal-leading-zero|lower-roman|upper-roman|lower-alpha|upper-alpha|lower-greek|lower-latin|upper-latin|hebrew|armenian|georgian|cjk-ideographic|hiragana|katakana|hiragana-iroha|katakana-iroha)'),
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' $1'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' $1'),
		),
		'before' => '<ul class="bbc_list{class}" style="list-style-type: {type};{style}">',
		'after' => '</ul>',
		'trim' => 'inside',
		'require_children' => array('li'),
		'block_level' => true,
	);
	$codes[] = array(
		'tag' => 'span',
		'parameters' => array(
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' class="$1"'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'before' => '<span{class}{style}>',
		'after' => '</span>',
	);
	$codes[] = array(
		'tag' => 'table',
		'parameters' => array(
			'border' => array('optional' => true, 'value' => ' border="$1"', 'match' => '(\d+)'),
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' $1'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'before' => '<table class="bbc_table{class}"{border}{style}>',
		'after' => '</table>',
		'trim' => 'inside',
		'require_children' => array('tr'),
		'block_level' => true,
	);
	$codes[] = array(
		'tag' => 'td',
		'parameters' => array(
			'colspan' => array('optional' => true, 'value' => ' colspan="$1"', 'match' => '(\d+)'),
			'rowspan' => array('optional' => true, 'value' => ' rowspan="$1"', 'match' => '(\d+)'),
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' class="$1"'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'before' => '<td{colspan}{rowspan}{class}{style}>',
		'after' => '</td>',
		'require_parents' => array('tr'),
		'trim' => 'outside',
		'block_level' => true,
		'disabled_before' => '',
		'disabled_after' => '',
	);
	$codes[] = array(
		'tag' => 'tr',
		'parameters' => array(
			'class' => array('optional' => true, 'quoted' => true, 'value' => ' class="$1"'),
			'style' => array('optional' => true, 'quoted' => true, 'value' => ' style="$1"'),
		),
		'before' => '<tr{class}{style}>',
		'after' => '</tr>',
		'require_parents' => array('table'),
		'require_children' => array('td'),
		'trim' => 'both',
		'block_level' => true,
		'disabled_before' => '',
		'disabled_after' => '',
	);
}

// Adds bbc buttons.
function bbcws_bbc_buttons(&$bbc_tags)
{
	global $txt;

	// Load language
	loadLanguage('BBCodeWithStyle');

	$bbc_tags[1][] = array();
	$bbc_tags[1][] = array(
		'image' => 'div',
		'code' => 'div',
		'before' => '[div style=""]',
		'after' => '[/div]',
		'description' => $txt['style_paragraph'],
	);
	$bbc_tags[1][] = array(
		'image' => 'span',
		'code' => 'span',
		'before' => '[span style=""]',
		'after' => '[/span]',
		'description' => $txt['style_character'],
	);
}

?>
